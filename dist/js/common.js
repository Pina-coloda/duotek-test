"use strict";

var monthsIm = ['январь', 'фервраль', 'март', 'апрель', 'май', 'июнь', 'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь'];
var monthsRod = ['января', 'фервраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'];

var groupBy = function groupBy(xs, key) {
  return xs.reduce(function (rv, x) {
    (rv[key(x)] = rv[key(x)] || []).push(x);
    return rv;
  }, {});
};

$(document).ready(function () {
  $('.header__nav-icon').on('click', function () {
    $(this).toggleClass('header__nav-active');
    $('.header__nav').fadeToggle();
    $('.profile').fadeToggle();
  });
  $('.header__nav__item').on('click', function () {
    $('.header__nav__item').removeClass('header__nav__item-active');
    $(this).addClass('header__nav__item-active');
  });
  $('.section-btn').on('click', function () {
    $('.section-btn').removeClass('section-btn-active');
    $(this).addClass('section-btn-active');
  });
  $(document).on('click', function (e) {
    if (!$(e.target).closest(".record__settings").length) {
      $('.settings__list').fadeOut();
    }

    e.stopPropagation();
  });
  var eventList = new Vue({
    el: '#eventList',
    data: {
      eventGroups: groupBy(records, function (x) {
        return monthsIm[x.date.getMonth()] + ' ' + x.date.getFullYear().toString();
      })
    },
    methods: {
      toggleList: function toggleList(event) {
        $(event.currentTarget).find('.settings__list').fadeToggle();
      }
    }
  });
});