'use strict'

const gulp         = require('gulp'),
	  pug          = require('gulp-pug'),
	  autoprefixer = require('gulp-autoprefixer'),
	  concat       = require('gulp-concat'),
	  uglify       = require('gulp-uglifyjs'),
	  cssnano      = require('gulp-cssnano'),
	  rename       = require('gulp-rename'),
	  del          = require('del'),
	  imagemin     = require('gulp-imagemin'),
	  pngquant     = require('imagemin-pngquant'),
	  cache        = require('gulp-cache'),
	  browserSync  = require('browser-sync').create(),
	  fs		   = require('fs'),
	  plumber 	   = require("gulp-plumber"),
	  babel        = require('gulp-babel'),
	  sass         = require("gulp-sass"),
	  sassGlob     = require('gulp-sass-glob');

const { task, watch, src, dest, series, parallel } = gulp;

//const projectList = JSON.parse(fs.readFileSync('app/js/project.json'));

function pugCom(cb){
	src('app/pug/**/*.pug')
		.pipe(pug({
			pretty: true,
			//locals: {projects: projectList}
		}))
		.pipe(dest('app'))
		.on('end', browserSync.reload);
	cb();
}

function cssLibs() {
	src('app/scss/libs.scss')
		.pipe(cssnano())
		.pipe(rename({suffix: '.min'}))
		.pipe(dest('app/css'))
}

function scssCom(cb) {
	src('app/scss/*.scss')
		.pipe(plumber())
		.pipe(sassGlob())
		.pipe(sass({ outputStyle: 'compressed' }))
		.on("error", sass.logError)
		.pipe(autoprefixer({
			cascade: true
		}))
		.pipe(dest('app/css'))
		.pipe(browserSync.stream());
	cb();
}

function scripts (cb) {
	src(['app/libs/jquery/dist/jquery.min.js',
		'app/libs/vue/dist/vue.min.js'])
		.pipe(concat('libs.min.js'))
		.pipe(uglify())
		.pipe(dest('app/js'));
	cb();
}

function serve(cb) {
	browserSync.init({
		server: 'app',
		notify: false
	});
	cb();
}

watch(['app/scss/**/*.scss'], series(scssCom, cssLibs));
watch(['app/pug/**/*.pug'], pugCom);
watch(['app/js/**/*.js']).on('change', browserSync.reload);

function clean (cb) {
	del.sync('dist');
	cb();
}

function img (cb) {
	src('app/img/**/*')
		.pipe(cache(imagemin({
			interlaced: true,
			progressive: true,
			svgoPlugins: [{removeViewBox: false}],
			use: [pngquant()]
		})))
		.pipe(dest('dist/img'));
	cb();
}

function build(cb) {
	src([
		'app/css/**/*.css'
		])
		.pipe(dest('dist/css'));

	src([
		'app/*.html'
		])
		.pipe(dest('dist'));

	src([
		'app/js/**/*.js'
		])
		.pipe(babel({
			presets: ['@babel/env']
		}))
		.pipe(dest('dist/js'));
	cb();
}

task(pugCom);
task(scssCom);
task(scripts);
task(serve);
task(build);
task(clean);
task(img);

exports.default = series(
	parallel(pugCom, scssCom, scripts),
	serve
	);

exports.build = series(img, clean, pugCom, scssCom, scripts, build);

