const records = [
    {
        date: new Date(2018, 9, 15),
        timeFrom: '18:00',
        timeTo: '20:00',
        avatar: 'img/ava2.png',
        flag: 'img/ger.png',
        men: {
            name: 'Иван Сокольский',
            description: '24 года, Россия, среднее образование'
        },
        eventType: 'Беседа',
        eventIcon: 'img/ger.png',
        isAgreed: true,
        isSearch: false
    },
    {
        date: new Date(2018, 9, 17),
        timeFrom: '18:00',
        timeTo: '20:00',
        avatar: 'img/ava3.png',
        flag: 'img/uzerpic.png',
        men: {
            name: 'Константин Константинопольский',
            description: '24 года, Россия, Высшее образование, Профессиональный репетитор'
        },
        eventType: 'Урок',
        eventIcon: 'img/rus.png',
        isAgreed: true,
        isSearch: false
    },
    {
        date: new Date(2018, 9, 17),
        timeFrom: '19:00',
        timeTo: '20:00',
        avatar: 'img/ava4.png',
        flag: 'img/ger.png',
        men: {
            name: 'Анна Петрова',
            description: '24 года, Россия, среднее образование'
        },
        eventType: 'Беседа',
        eventIcon: 'img/eng.png',
        isAgreed: true,
        isSearch: false
    },
    {
        date: new Date(2018, 10, 15),
        timeFrom: '18:00',
        timeTo: '20:00',
        avatar: 'img/tel.png',
        flag: 'img/ger.png',
        men: {
            name: 'Поиск собеседника',
            description: 'любой пол среднее образование'
        },
        eventType: 'Беседа',
        eventIcon: 'img/ger.png',
        isAgreed: true,
        isSearch: true
    },
    {
        date: new Date(2018, 10, 17),
        timeFrom: '18:00',
        timeTo: '20:00',
        avatar: 'img/ava5.png',
        flag: 'img/uzerpic.png',
        men: {
            name: 'Катерина Иванова',
            description: '44 года, Россия, Высшее образование, Профессиональный репетитор'
        },
        eventType: 'Урок',
        eventIcon: 'img/rus.png',
        isAgreed: false,
        isSearch: false
    }
];